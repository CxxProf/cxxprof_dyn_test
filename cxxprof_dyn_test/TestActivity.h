
#pragma once

#include <string>

#include "cxxprof_dyn_test/common.h"
#include <cxxprof_preloader/IActivity.h>

namespace CxxProf
{

    class CxxProf_Dyn_Test_EXPORT TestActivity : public IActivity
    {
    public:
        TestActivity(const std::string& name);
        virtual ~TestActivity();

    private:
        std::string name_;
    };

}
