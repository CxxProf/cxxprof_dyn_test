
#pragma once

#ifdef WIN32
	#ifdef CXXPROF_DYN_TEST
	    #define CxxProf_Dyn_Test_EXPORT __declspec( dllexport )
	#else
	    #define CxxProf_Dyn_Test_EXPORT __declspec( dllimport )
	#endif //CXXPROF_DYN_TEST
#else
	#define CxxProf_Dyn_Test_EXPORT 
#endif
